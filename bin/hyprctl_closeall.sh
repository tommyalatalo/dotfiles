#!/bin/zsh

for c in $(hyprctl clients -j | jq -r '.[].initialClass' | rg -v Alacritty); do
    hyprctl dispatch closewindow $c
done
