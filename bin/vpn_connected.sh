#!/bin/bash

conns=""

ip a | rg tun0 >/dev/null
if [ $? -eq 0 ]; then
    conns+="VPN "
fi

if [ -n "$conns" ]; then
    echo "$conns"
else
    printf '\u2589%.0s' {1..10}
fi
