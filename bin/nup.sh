#!/bin/bash
set +x

svc=$(git status --short --porcelain | rg hcl | awk '{print $NF}' | xargs -I% basename % | fzf)
echo $svc
[[ "$svc" == "" ]] && exit 1
tags=$(rg 'image\s+=' "$svc" | cut -d':' -f2 | tr -d '"')
[[ "$tags" == "" ]] && exit 1
version=$(echo "$tags" | fzf)

if [ "$svc" != "" ] && [ "$version" != "" ]; then
    read -p "Commit "${svc%.*}" update to $version? " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        echo
        git restore --staged .
        git add "${svc}"
        git commit -m "nomad: update ${svc%.*} to $version"
    fi
fi
