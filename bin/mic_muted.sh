#!/bin/bash

if [[ "$(pactl get-source-mute @DEFAULT_SOURCE@)" == *"yes"* ]]; then
    state="muted"
fi
echo $state
