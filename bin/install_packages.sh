#!/bin/bash

dotfiles="$(git rev-parse --show-toplevel)"
packages="${dotfiles}/assets/packages"
installed="/tmp/installed"

yay -Qq >$installed
missing="$(comm -13 <(sort -u "$installed") <(sort -u "$packages"))"

if [ -n "$missing" ]; then
    echo "installing missing packages:"
    printf "%s\n\n" "$missing"
    yay -S - <<<"$missing"
else
    echo "no packages to install"
fi
