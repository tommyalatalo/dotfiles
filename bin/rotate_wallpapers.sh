#!/bin/zsh

while true; do
    export f="$(fd . /home/tommy/Pictures/wallpapers/lashman/gruvbox | shuf -n1)"
    hyprctl hyprpaper preload $f
    hyprctl hyprpaper wallpaper "DP-1,$f"
    hyprctl hyprpaper unload all
    sleep 5m
done
