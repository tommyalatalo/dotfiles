-- set leader keys before loading lazy
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- initialize lazy plugin manager --
require("config.lazy")

-- load user configurations --
require("config.options")
require("config.keymaps")
