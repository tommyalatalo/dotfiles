return {
  -- general
  "nvim-lua/plenary.nvim", -- useful lua functions used by lots of plugins
  "nvim-lua/popup.nvim",  -- an implementation of the popup api from vim in neovim

  -- editor customizations
  {
    "rmagatti/alternate-toggler", -- toggle boolean values etc
    keys = {
      { "<leader>tn", ":ToggleAlternate<CR>" },
    },
  },
  "farmergreg/vim-lastplace", -- restore buffer at last edit position
  "godlygeek/tabular",       -- align text by column
  "machakann/vim-swap",      -- reorder delimited items
  "svban/YankAssassin.vim",  -- keep cursor position when yanking
  "tpope/vim-abolish",       -- easily search for and replace multiple variants of words
  "tpope/vim-endwise",       -- automatically end code structures
  "tpope/vim-eunuch",        -- add basic unix tools to vim command line
  "tpope/vim-repeat",        -- remaps . so that other plugins can tap into it
  "tpope/vim-sleuth",        -- automatic indentation handling and editorconfig
  "tpope/vim-speeddating",   -- intelligent date incrementation and formatting

  -- additional features
  "dbeniamine/cheat.sh-vim", -- cheat.sh in vim
  "tpope/vim-fugitive",     -- a git plugin so awesome it should be illegal

  -- lsp
  "neovim/nvim-lspconfig",          -- enable LSP
  "williamboman/nvim-lsp-installer", -- simple to use language server installer
  "tamago324/nlsp-settings.nvim",   -- language server settings defined in json format
  "onsails/lspkind.nvim",           -- icons for lsp   Not Committed Yet

  -- snippets
  "L3MON4D3/LuaSnip",            -- snippet engine
  "rafamadriz/friendly-snippets", -- collection of snippets
}
