vim.g.gitblame_enabled = 0
return {
	"f-person/git-blame.nvim",
	keys = { { "<leader>b", "<cmd>GitBlameToggle<CR>" } },
}
