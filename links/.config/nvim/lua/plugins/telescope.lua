return {
	"nvim-telescope/telescope.nvim",
	dependencies = {
		{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
	},
	lazy = false,
	keys = {
		{
			"<leader>df",
			function()
				require("telescope.builtin").find_files({
					prompt_title = "Dotfiles",
					cwd = "~/dotfiles/",
					find_command = { "fd", "--type", "f", "--exclude", ".git/*", "--hidden" },
				})
			end,
		},
		{
			"<leader>gb",
			function()
				local actions = require("telescope.actions")
				require("telescope.builtin").git_branches({
					attach_mappings = function(prompt_bufnr, map)
						map("i", "<c-d>", actions.git_delete_branch)
						map("n", "<c-d>", actions.git_delete_branch)
						return true
					end,
				})
			end,
		},
		{
			"<leader>ff",
			function()
				require("telescope.builtin").find_files({
					find_command = { "fd", "--type", "f", "--exclude", ".git/*", "--hidden" },
				})
			end,
		},
		{
			"<leader>fb",
			function()
				require("telescope.builtin").buffers({})
			end,
		},
		{
			"<leader>fg",
			function()
				require("telescope.builtin").live_grep({ hidden = false })
			end,
		},
		{
			"<leader>fk",
			function()
				require("telescope.builtin").keymaps({})
			end,
		},
		{
			"<leader>gs",
			function()
				require("telescope.builtin").git_status({})
			end,
		},
		{
			"<leader>gl",
			function()
				require("telescope.builtin").git_commits({ sorting_strategy = "ascending" })
			end,
		},
		{
			"<leader>gvf",
			function()
				require("telescope.builtin").git_bcommits({})
			end,
		},
		{
			"<leader>/",
			function()
				require("telescope.builtin").current_buffer_fuzzy_find({})
			end,
		},
	},
	config = function()
		local telescope = require("telescope")
		telescope.setup(_, opts)
		telescope.load_extension("fzf")
	end,
	opts = {
		defaults = {
			vimgrep_arguments = {
				"rg",
				"--color=never",
				"--column",
				"--hidden",
				"--line-number",
				"--no-heading",
				"--smart-case",
				"--with-filename",
			},
			prompt_prefix = " ❯ ",
			selection_caret = "❯ ",
			entry_prefix = "  ",
			initial_mode = "insert",
			selection_strategy = "reset",
			sorting_strategy = "descending",
			layout_strategy = "horizontal",
			layout_config = {
				horizontal = {
					mirror = false,
				},
				vertical = {
					mirror = false,
				},
				prompt_position = "bottom",
				width = 0.8,
				preview_cutoff = 120,
			},
			mappings = {
				i = {
					["<esc>"] = function(...)
						return require("telescope.actions").close(...)
					end,
				},
			},
			path_display = { "absolute" },
			file_sorter = require("telescope.sorters").get_fzy_sorter,
			generic_sorter = require("telescope.sorters").get_fzy_sorter,
			winblend = 0,
			border = {},
			borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
			color_devicons = true,
			use_less = true,
			set_env = { ["COLORTERM"] = "truecolor" },
			file_previewer = require("telescope.previewers").vim_buffer_cat.new,
			grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
			qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
		},
	},
}
