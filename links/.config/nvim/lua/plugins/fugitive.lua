return {
	"tpope/vim-fugitive",
	lazy = false,
	keys = {
		{ "<leader>gm", "<cmd>Gvdiffsplit!<CR>" },
		-- { "<leader>t", "<cmd>diffget //3<CR>" },
		-- { "<leader>n", "<cmd>diffget //3<CR>" },
	},
	config = function()
		if vim.wo.diff then
			vim.keymap.set("t", "<leader>t", "<cmd>diffget //2<cr>", { desc = "" })
			vim.keymap.set("n", "<leader>n", "<cmd>diffget //3<cr>", { desc = "" })
		end
	end,
}
