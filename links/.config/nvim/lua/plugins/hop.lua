vim.cmd([[
  augroup hophighlights
    highlight HopUnmatched guifg=#665c54
    highlight HopNextKey guifg=#fabd2f
    highlight HopNextKey1 guifg=#fabd2f
    highlight HopNextKey2 guifg=#d79921
  augroup end
]])

return {
	"phaazon/hop.nvim",
	keys = {
		{ "<leader>ho", "<cmd>HopChar1<CR>" },
		{ "<leader>ht", "<cmd>HopChar2<CR>" },
		{ "<leader>hl", "<cmd>HopLine<CR>" },
		{ "<leader>hw", "<cmd>HopWord<CR>" },
	},
	opts = {
		keys = "arstneiowfuy",
		term_seq_bias = 0.5,
	},
}
