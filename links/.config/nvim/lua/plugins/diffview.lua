return {
	{
		"sindrets/diffview.nvim", -- diff viewer
		keys = {
			{ "<leader>do", ":DiffviewOpen<CR>" },
			{ "<leader>dc", ":DiffviewClose<CR>" },
		},
	},
}
