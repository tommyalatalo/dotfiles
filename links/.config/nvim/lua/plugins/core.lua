return {
	{ "rcarriga/nvim-notify", enabled = false },
	{ "folke/noice.nvim", enabled = false },
	{ "ggandor/leap.nvim", enabled = false },
	{ "ggandor/flit.nvim", enabled = false },
	{ "akinsho/bufferline.nvim", enabled = false },
	{ "echasnovski/mini.pairs", enabled = false },
	{
		"echasnovski/mini.indentscope",
		enabled = true,
		version = false, -- wait till new 0.7.0 release to put it back on semver
		event = "BufReadPre",
		opts = {
			symbol = "│",
			options = { try_as_border = true },
			draw = {
				-- Delay (in ms) between event and start of drawing scope indicator
				delay = 0,
				-- Animation rule for scope's first drawing. A function which, given
				-- next and total step numbers, returns wait time (in ms). See
				-- |MiniIndentscope.gen_animation| for builtin options. To disable
				-- animation, use `require('mini.indentscope').gen_animation.none()`.
				animation = require("mini.indentscope").gen_animation.none(),
			},
		},
	},
	{
		"echasnovski/mini.surround",
		keys = { "gs" },
		opts = {
			mappings = {
				add = "gsa", -- Add surrounding in Normal and Visual modes
				delete = "gsd", -- Delete surrounding
				find = "gsf", -- Find surrounding (to the right)
				find_left = "gsF", -- Find surrounding (to the left)
				highlight = "gsh", -- Highlight surrounding
				replace = "gsr", -- Replace surrounding
				update_n_lines = "gsn", -- Update `n_lines`
			},
		},
	},
	{
		"folke/flash.nvim",
		event = "VeryLazy",
		vscode = true,
		---@type Flash.Config
		opts = {},
    -- stylua: ignore
    keys = {
      { "s", mode = { "n", "x", "o" }, false },
      { "S", mode = { "n", "x", "o" }, false },
      { "<leader>s", mode = { "n", "x", "o" }, function() require("flash").jump() end, desc = "Flash" },
      { "<leader>V", mode = { "n", "o", "x" }, function() require("flash").treesitter() end, desc = "Flash Treesitter" },
      { "r", mode = "o", function() require("flash").remote() end, desc = "Remote Flash" },
      { "R", mode = { "o", "x" }, function() require("flash").treesitter_search() end, desc = "Treesitter Search" },
      { "<c-s>", mode = { "c" }, function() require("flash").toggle() end, desc = "Toggle Flash Search" },
    },
	},
	{
		"folke/todo-comments.nvim",
		dependencies = { { "nvim-lua/plenary.nvim" } },
		keys = {
			{ "<leader>ft", "<cmd>TodoTelescope keywords=TODO<CR>" },
		},
		opts = {
			keywords = {
				TODO = { icon = " ", color = "todo" },
			},
			colors = {
				todo = { "DiagnosticWarn", "WarningMsg", "#fabd2f" },
			},
			gui_style = {
				fg = "BOLD", -- The gui style to use for the fg highlight group.
				bg = "BOLD", -- The gui style to use for the bg highlight group.
			},
		},
	},
}
