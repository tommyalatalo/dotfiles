return {
	"stevearc/conform.nvim",
	opts = {
		format = {
			timeout_ms = 3000,
			async = false, -- not recommended to change
			quiet = false, -- not recommended to change
		},
		formatters_by_ft = {
			hcl = { "terraform_fmt" },
			lua = { "stylua" },
			sh = { "shfmt" },
		},
		formatters = {
			injected = { options = { ignore_errors = true } },
			shfmt = {
				prepend_args = { "--indent", "4" },
			},
		},
	},
}
