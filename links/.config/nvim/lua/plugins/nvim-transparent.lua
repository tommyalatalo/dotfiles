vim.g.transparent_enabled = true

return {
	"xiyaowong/nvim-transparent",
	lazy = false,
	opts = {
		extra_groups = {}, -- table/string: additional groups that should be cleared
		exclude_groups = {}, -- table: groups you don't want to clear
	},
}
