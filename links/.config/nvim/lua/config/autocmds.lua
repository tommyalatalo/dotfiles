local function augroup(name)
	return vim.api.nvim_create_augroup("lazyvim_" .. name, { clear = true })
end

vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
	desc = "Set helm filetype",
	pattern = {
		"*/templates/*.yaml",
		"*/templates/*.tpl",
		"*.gotmpl",
		"helmfile*.yaml",
	},
	callback = function()
		vim.opt_local.filetype = "helm"
	end,
})

vim.api.nvim_create_autocmd("FileType", {
	desc = "Change commentstring for helm files",
	group = augroup("commentstring"),
	pattern = { "helm" },
	callback = function()
		vim.opt.commentstring = "# %s"
	end,
})

-- customize timeout for yank highlight
vim.api.nvim_create_autocmd("TextYankPost", {
	group = augroup("highlight_yank"),
	callback = function()
		vim.highlight.on_yank({ timeout = 100 })
	end,
})
