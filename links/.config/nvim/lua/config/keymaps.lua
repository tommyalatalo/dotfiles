local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

-- shorten function name
local keymap = vim.api.nvim_set_keymap

--remap space as leader key
keymap("", "<Space>", "<Nop>", opts)

-- modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- normal --
-- source init.lua
keymap("n", "<leader><leader>s", ":source ~/dotfiles/links/.config/nvim/init.lua<CR>", opts)

-- replace without overwriting implicit registry
keymap("n", "<leader>p", "viwP", opts)

-- move text up and down
keymap("n", "<C-Down>", ":m .+1<CR>==", opts)
keymap("n", "<C-Up>", ":m .-2<CR>==", opts)

-- search and replace word
keymap("n", "cn", "*``cgn", opts)
keymap("n", "cN", "*``cgN", opts)

-- yank to end of line
keymap("n", "Y", "y$", opts)

-- keep cursor centered
keymap("n", "n", "nzzzv", opts)
keymap("n", "N", "Nzzzv", opts)
keymap("n", "J", "mzJ`j", opts)
keymap("n", "<C-j>", ":cnext<CR>zzzv", opts)

-- insert --
-- move text up and down
keymap("i", "<C-Down>", "<esc>:m .+1<CR>==", opts)
keymap("i", "<C-Up>", "<esc>:m .-2<CR>==", opts)

-- break points for undo operations
keymap("i", ",", ",<c-g>u", opts)
keymap("i", ".", ".<c-g>u", opts)
keymap("i", "!", "!<c-g>u", opts)
keymap("i", "?", "?<c-g>u", opts)
keymap("i", "[", "[<c-g>u", opts)
keymap("i", "]", "]<c-g>u", opts)
keymap("i", "{", "{<c-g>u", opts)
keymap("i", "}", "}<c-g>u", opts)
keymap("i", "(", "(<c-g>u", opts)
keymap("i", ")", ")<c-g>u", opts)
keymap("i", "=", "=<c-g>u", opts)

-- visual --
-- move text up and down
keymap("v", "<C-Down>", ":m '>+1<CR>gv=gv", opts)
keymap("v", "<C-Up>", ":m '<-2<CR>gv=gv", opts)
keymap("v", "p", '"_dP', opts)

-- stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- visual block --
-- enable dot operator in visual block mode
keymap("x", ".", ":normal .<CR>", opts)

-- move text up and down
keymap("x", "<C-Down>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<C-Up>", ":move '<-2<CR>gv-gv", opts)

-- terminal --
-- better terminal navigation
keymap("t", "<C-Left>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-Down>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-Up>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-Right>", "<C-\\><C-N><C-w>l", term_opts)

-- plugins --
-- lsp
keymap("i", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
keymap("n", "<leader><leader>ds", "<cmd>lua vim.diagnostic.show()<CR>", opts)
keymap("n", "<leader><leader>dh", "<cmd>lua vim.diagnostic.hide()<CR>", opts)
