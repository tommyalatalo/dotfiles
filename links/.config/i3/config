# i3 config file (v4)
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

# Mod1 Alt
# Mod2 Numlock
# Mod3 Empty
# Mod4 Super
set $alt Mod1
set $numlock Mod2
set $super Mod4

# gaps
gaps inner 4
gaps outer 1
smart_gaps on
smart_borders on
hide_edge_borders smart

# disable borders
new_window 1pixel
for_window [class=".*"] border pixel 1

# mouse warping
mouse_warping none

# set mouse hovering focus
focus_follows_mouse no

# focus window on activation smart|urgent|focus|none
focus_on_window_activation focus

# font for window titles. will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Noto Sans Medium 7

# this font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $alt

# kill focused window
bindsym $alt+Shift+q kill
bindsym $super+c kill

# change focus
bindsym $super+h          focus left
bindsym $super+j          focus down
bindsym $super+k          focus up
bindsym $super+l          focus right
bindsym $super+n          focus left
bindsym $super+e          focus down
bindsym $super+i          focus up
bindsym $super+o          focus right

# alternatively, you can use the cursor keys:
bindsym $super+Left       focus left
bindsym $super+Down       focus down
bindsym $super+Up         focus up
bindsym $super+Right      focus right

# move focused window
bindsym $alt+Shift+h    move left
bindsym $alt+Shift+j    move down
bindsym $alt+Shift+k    move up
bindsym $alt+Shift+l    move right

# alternatively, you can use the cursor keys:
bindsym $alt+Shift+Left  move left
bindsym $alt+Shift+Down  move down
bindsym $alt+Shift+Up    move up
bindsym $alt+Shift+Right move right

# switch between the current and the previously focused one
bindsym $super+$alt+p workspace back_and_forth
bindsym $super+x move container to workspace back_and_forth

# move to numbered workspaces
bindsym $alt+Shift+KP_End   move container to workspace number $ws-browser, focus
bindsym $alt+Shift+KP_Down  move container to workspace number $ws-mail, focus
bindsym $alt+Shift+KP_Next  move container to workspace number $ws-chat, focus
bindsym $alt+Shift+KP_Left  move container to workspace number $ws-editor, focus
bindsym $alt+Shift+KP_Begin move container to workspace number $ws-graphics, focus
bindsym $alt+Shift+KP_Right move container to workspace number $ws-video, focus
bindsym $alt+Shift+KP_Home  move container to workspace number $ws-blender, focus
bindsym $alt+Shift+KP_Up    move container to workspace number $ws-virtualbox, focus
bindsym $alt+Shift+KP_Prior move container to workspace number $ws-steam, focus

# split in horizontal orientation
bindsym $alt+h split h

# split in vertical orientation
bindsym $alt+v split v

# enter fullscreen mode for the focused container
bindsym $alt+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# bindsym $alt+s layout stacking
bindsym $alt+w layout tabbed
bindsym $alt+s layout toggle split

# toggle tiling / floating
bindsym $alt+Shift+space floating toggle

# change focus between tiling / floating windows
# bindsym $alt
#+space focus mode_toggle

# focus the parent container
# bindsym $alt+a focus parent

# focus the child container
# bindsym $alt+d focus child

# launchers
bindsym Control+Shift+Escape exec xfce4-taskmanager
bindsym $super+f exec nautilus
bindsym --release Super_L exec $HOME/.config/rofi/launchers/misc/launcher.sh
bindsym --release $super+Shift+c exec $HOME/.config/rofi/launchers/misc/launcher.sh calc
bindsym --release $super+v exec $HOME/.config/rofi/launchers/misc/launcher.sh clipboard
bindsym --release $super+t exec $HOME/.config/rofi/launchers/misc/launcher.sh todo
bindsym Print exec flameshot screen --clipboard
bindsym Shift+Print exec flameshot gui

# audio controls
bindsym $super+M exec pactl set-source-mute @DEFAULT_SOURCE@ toggle && notify-send -t 1000 "$(pactl get-source-mute @DEFAULT_SOURCE@ | tr y Y | tr n N)"

# move workspace between monitors
bindsym $alt+Control+Shift+Left  move workspace to output left
bindsym $alt+Control+Shift+Right move workspace to output left

# keymap for floating alacritty
bindsym Control+Shift+$alt+$super+T exec tdrop -w 90% -h 94% -x 5% -y 3% -mts primary alacritty
bindsym XF86Open exec tdrop -w 90% -h 94% -x 5% -y 3% -mts primary alacritty
# keymap for floating bitwarden
bindsym XF86Tools exec tdrop -w 60% -h 94% -x 20% -y 3% -mts primary bitwarden-desktop

# floating windows
for_window [class="Alacritty"] floating enable
for_window [class="Bitwarden"] floating enable
for_window [class="Gnome-calculator"] floating enable
for_window [class="evolution-alarm-notify"] floating enable
for_window [title="Microsoft Teams Notification"] floating enable

# tabbed layouts
bindsym $alt+x layout toggle splith tabbed #stacking

# define names for default workspaces
#left monitor
set $ws-wpleft                  "0"

#right monitor
set $ws-wpright                 "0"
set $ws-browser                 "1Browser "
set $ws-mail                    "2Mail "
set $ws-chat                    "3Chat "
set $ws-audio                   "4Audio "
set $ws-video                   "5Video "
set $ws-editor                  "6Editor "
set $ws-graphics                "7Graphics "
set $ws-zenkit                  "8Zenkit "
set $ws-deluge                  "*Deluge "
set $ws-virtualbox              "*VirtualBox "
set $ws-steam                   "*Steam "
set $ws-substancepainter        "*Substance Painter "
set $ws-substancedesigner       "*Substance Designer "
set $ws-calibre                 "*Calibre "
set $ws-cura                    "*Cura "
set $ws-slicer                  "*Slicer "

# assign application workspaces
assign [class="Blender"]                    $ws-graphics
assign [class="Brave-browser"]              $ws-browser
assign [class="CHITUBOX"]                   $ws-slicer
assign [class="Calibre"]                    $ws-calibre
assign [class="Deluge"]                     $ws-deluge
assign [class="Evolution"]                  $ws-mail
assign [class="Ferdi"]                      $ws-chat
assign [class="Gimp-2.10"]                  $ws-graphics
assign [class="Kicad"]                      $ws-editor
assign [class="Kodi"]                       $ws-video
assign [class="Navigator"]                  $ws-browser
assign [class="Microsoft Teams - Preview"]  $ws-chat
assign [class="Opera"]                      $ws-browser
assign [class="Rhythmbox"]                  $ws-audio
assign [class="Signal"]                     $ws-chat
assign [class="Slack"]                      $ws-chat
assign [class="Spotify"]                    $ws-audio
assign [class="Steam"]                      $ws-steam
assign [class="Sublime-music"]              $ws-audio
assign [class="Substance Designer"]         $ws-substancedesigner
assign [class="Substance Painter"]          $ws-substancepainter
assign [class="Tauon Music Box"]            $ws-audio
assign [class="VirtualBox Machine"]         $ws-virtualbox
assign [class="WebCord"]                    $ws-chat
assign [class="celluloid"]                  $ws-video
assign [class="code"]                       $ws-editor
assign [class="cura"]                       $ws-cura
assign [class="discord"]                    $ws-chat
assign [class="firefox"]                    $ws-browser
assign [class="krita"]                      $ws-graphics
assign [class="lycheeslicer"]               $ws-slicer
assign [class="mpv"]                        $ws-video
assign [class="obs"]                        $ws-video
assign [class="obsidian"]                   $ws-editor
assign [class="org.inkscape.Inkscape"]      $ws-graphics
assign [class="prusa-slicer"]               $ws-slicer
assign [class="sayonara"]                   $ws-audio
assign [class="thunderbird"]                $ws-mail
assign [class="tidal-hifi"]                 $ws-audio
assign [class="vlc"]                        $ws-video
assign [class="zenkit"]                     $ws-zenkit

# move stubborn applications
for_window [class="Brave-browser"]          move to workspace $ws-browser
for_window [class="Navigator"]              move to workspace $ws-browser
for_window [class="Librewolf"]              move to workspace $ws-browser
for_window [class="Inkscape"]               move to workspace $ws-graphics
for_window [class="Kicad"]                  move to workspace $ws-editor
for_window [class="Kodi"]                   move to workspace $ws-video
for_window [class="Spotify"]                move to workspace $ws-audio
for_window [class="VirtualBox Machine"]     move to workspace $ws-virtualbox
for_window [class="Zenkit"]                 move to workspace $ws-zenkit
for_window [class="firefox"]                move to workspace $ws-browser
for_window [class="obsidian"]               move to workspace $ws-editor
for_window [class="org.inkscape.Inkscape"]  move to workspace $ws-graphics
for_window [class="prusa-slicer"]           move to workspace $ws-slicer

# assign workspaces to monitors
Set $primary   DP1
Set $secondary DP2

# Bind the workspaces for left monitor
workspace $ws-wpleft           output $secondary

# Bind the workspaces for right monitor
workspace $ws-audio            output $primary
workspace $ws-blender          output $primary
workspace $ws-browser          output $primary
workspace $ws-chat             output $primary
workspace $ws-deluge           output $primary
workspace $ws-editor           output $primary
workspace $ws-graphics         output $primary
workspace $ws-mail             output $primary
workspace $ws-steam            output $primary
workspace $ws-video            output $primary
workspace $ws-virtualbox       output $primary
workspace $ws-wpright          output $primary

# focus application workspace on launch
for_window [class=(?i)Bitwarden] focus
for_window [class=(?i)Blender] focus
for_window [class=(?i)Deluge] focus
for_window [class=(?i)Firefox] focus
for_window [class=(?i)Inkscape] focus
for_window [class=(?i)Kodi] focus
for_window [class=(?i)Kodi] fullscreen
for_window [class=(?i)Nemo] focus
for_window [class=(?i)Opera] focus
for_window [class=(?i)Spotify] focus
for_window [class=(?i)Thunderbird] focus
for_window [class=(?i)Transgui] focus
for_window [class=(?i)celluloid] focus
for_window [class=(?i)mpv] focus
for_window [class=(?i)code] focus
for_window [class=(?i)discord] focus
for_window [class=(?i)sayonara] focus
for_window [class=(?i)Zenkit] focus

## switch workspaces
bindsym $alt+0          workspace       $ws-wpright
bindsym $alt+1          workspace       $ws-browser
bindsym $alt+2          workspace       $ws-mail
bindsym $alt+3          workspace       $ws-chat
bindsym $alt+4          workspace       $ws-audio
bindsym $alt+5          workspace       $ws-video
bindsym $alt+6          workspace       $ws-editor
bindsym $alt+7          workspace       $ws-graphics
bindsym $alt+8          workspace       $ws-zenkit
bindsym $alt+9          workspace       $ws-steam

# reload the configuration file
bindsym $alt+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $alt+Shift+r restart
bindsym $alt+Shift+e exec xfce4-session-logout

# resize window (you can also use the mouse for that)
bindsym $super+r mode "resize"
mode "resize" {
        # these bindings trigger as soon as you enter the resize mode
        bindsym k resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $alt+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $super+r mode "default"
}

# Define colors
# class                 border  backgr. text    indicator child_border
client.focused          #4c7899 #606060 #ffffff #fabd2f   #b57614
client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
client.unfocused        #333333 #404040 #888888 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c
client.background       #ffffff

title_align center

# auto-started applications
exec --no-startup-id brave --password-store=basic
exec --no-startup-id webcord
exec --no-startup-id dunst
exec --no-startup-id flameshot
exec --no-startup-id greenclip daemon
exec --no-startup-id ksuperkey
exec --no-startup-id picom
exec --no-startup-id redshift-gtk
exec --no-startup-id signal-desktop
exec --no-startup-id syncthing-gtk
exec --no-startup-id thunderbird
exec --no-startup-id unclutter
exec --no-startup-id xbindkeys
exec --no-startup-id zenkit
exec --no-startup-id i3-msg 'workspace ws-browser; exec librewolf'
exec_always --no-startup-id feh --bg-center '/home/tommy/.config/wallpaper.jpg'
