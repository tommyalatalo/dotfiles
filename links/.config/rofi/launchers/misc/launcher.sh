#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

# Available Styles
# >> Created and tested on : rofi 1.6.0-1
#
# blurry	blurry_full		kde_simplemenu		kde_krunner		launchpad
# gnome_do	slingshot		appdrawer			appdrawer_alt	appfolder
# column	row				row_center			screen			row_dock		row_dropdown

theme="gruvbox-dark"
dir="$HOME/.config/rofi/launchers/misc"

# comment these lines to disable random style
# themes=($(ls -p --hide="launcher.sh" $dir))
# theme="${themes[$(( $RANDOM % 16 ))]}"

if [ "$1" = "calc" ]; then
    rofi -no-lazy-grab -theme "$theme" -kb-cancel Escape,Super_L -modi "calc" -show calc -run-command '{cmd}'
elif [ "$1" = "clipboard" ]; then
    rofi -no-lazy-grab -theme "$theme" -kb-cancel Escape,Super_L -modi "clipboard:greenclip print" -show clipboard -run-command '{cmd}'
elif [ "$1" = "todo" ]; then
    rofi -no-lazy-grab -theme "$theme" -kb-cancel Escape,Super_L -modi "drun,todo:todo.sh -p -d ~/.todo/config" -show todo -run-command '{cmd}'
else
    rofi -no-lazy-grab -theme "$theme" -kb-cancel Escape,Super_L -modi drun -combi-modi window,drun -show drun -display-drun ""
fi
