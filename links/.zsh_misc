#================================================
#                 Miscellaneous
#================================================

alias sz='source ~/.zshrc'

# bottom
alias btm='btm --color=gruvbox'
alias top='btm'

# broot
alias br='br -s'
alias bri='br -si'
alias brh='br -sh'
alias brhi='br -shi'

# hashistack acl
alias acl='source ~/dotfiles/bin/acl.sh'

# bliss
alias bup='cd ~/git/homelab/nomad/jobs && acl && nomad job run bliss.hcl && cd - &>/dev/null'
function blog() {
	acl
	nomad alloc logs -stderr -f -tail "$(nomad job status bliss | rg running | rg bliss | awk '{print $1}')" bliss
}
alias bvim='nvim ~/git/homelab/nomad/jobs/config/bliss/books.yml'

## switch to audio device
alias ee='~/dotfiles/bin/pactl.sh --output easyeffects'
alias sp='~/dotfiles/bin/pactl.sh --output speakers'
alias uv='~/dotfiles/bin/pactl.sh --input uv --output uv'
alias bt='~/dotfiles/bin/pactl.sh --input uv --output wh1000xm4 --profile ldac'
alias hp='~/dotfiles/bin/pactl.sh --input uv --output d90le'

## connect and switch to bluetooth audio device
function btp() {
	~/dotfiles/bin/bluetooth.sh --device F8:4E:17:66:90:72 --controller F0:2F:74:63:3A:12 --pair
	~/dotfiles/bin/pactl.sh --output wh1000xm4 --input uv --profile ldac
}

## zsh
alias zshconfig='sudo vim ~/.zshrc'
alias ohmyzsh='sudo vim ~/.oh-my-zsh'

## display switching
alias home='~/dotfiles/bin/switch_host.sh home'
alias work='~/dotfiles/bin/switch_host.sh work'
alias laptop='~/dotfiles/bin/switch_host.sh laptop'

# ripgrep
alias rgi='rg -i'
alias rgu='rg -uu'

## misc
alias cat='bat'
alias catp='bat -p'
alias clip='cliphist store'
alias df='df -h'
alias diff='colordiff' # requires colordiff package
alias dirs='dirs -v'
alias dmesg='dmesg -HL'
alias du='du -c -h'
alias grep='grep --color=auto'
alias h='history | rg -i'
alias mkdir='mkdir -p -v'
alias more='less'
alias n='nvim'
function nf() {
  f="$(fd --type f | fzf --preview 'bat --style=numbers --color=always {}' || true)"
  [[ $f != "" ]] && nvim "$f"
}
alias nano='nano -w'
alias o='xdg-open'
alias p='ps aux | rg -i'
alias ping='ping -c 5'
alias tree='tree -aC'
alias wttr='curl wttr.in'
alias q='qalc'
alias xc='xclip -selection c'
alias xco='xclip -o'
alias ~='cd ~'
function rrm() {
	if [ $# -ne 1 ]; then
		echo "rrm takes exactly one argument"
		return
	fi
	dir=$(echo "$1" | sed 's:/*$::')
	empty="$(mktemp)"
	rsync -a --delete --progress $dir $empty
	rm -rf $dir $empty
}

## go-task
alias t="go-task"

## ssh
alias backup='ssh void@backup'
alias nas='ssh void@nas'

## ls
alias l='eza -laghHF --git --group-directories-first'  # list view
alias lt='eza -laghHTF --git -I ".terra*"'             # tree view
alias ls='eza -xaF'

## safety features
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -I'                    # 'rm -i' prompts for every file
## safer alternative w/ timeout, not stored in history
alias rm=' timeout 3 rm -Iv --one-file-system'
alias ln='ln -i'
alias chown='sudo chown --preserve-root'
alias chmod='sudo chmod --preserve-root'
alias chgrp='sudo chgrp --preserve-root'

## make bash error tolerant
alias :q=' exit'
alias :Q=' exit'
alias :x=' exit'

## navigation
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
